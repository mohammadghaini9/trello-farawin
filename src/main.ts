import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './style/index.scss'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

const app = createApp(App)

app.use(router)

app.directive('autofocus', {
  mounted(el) {
    el.focus()
  },
})

app.mount('#app')
