import { get, post } from '@/utils/http'
import { ref, readonly } from 'vue'

const _boards = ref([])

// get Boards of server
export function fetchBoards(userID: string, userRole: string) {
  // if (userRole === '1') {
  //   get('/getBoards').then(res => {
  //     if (res.success) {
  //       _boards.value = res.boards
  //     } else {
  //       _boards.value = []
  //     }
  //   })
  // } else {
  post('/getUserBoards', {
    userID: userID,
    userRole: userRole
  }).then(res => {
    if (res.success) {
      _boards.value = res.boards
    } else {
      _boards.value = []
    }
  })
  // }
}

export function addBoard(boardName: string, boardDesc: string, userID: string, userRole: string) {
  post('/createBoard', {
    name: boardName,
    desc: boardDesc
  }).then(res => {
    if (res.success) {
      fetchBoards(userID, userRole)
      setTimeout(() => alert('بورد با موفقیت ایجاد شد'), 100)
    } else {
      setTimeout(() => alert('ایجاد بورد با خطا مواجه شد'), 50)
    }
  })
}

export function updateBoardName(newName: string, desc: string, id: string, userID: string, userRole: string) {
  post('/updateBoard', {
    name: newName,
    desc: desc,
    id: id,
  }).then(res => {
    if (res.success) {
      fetchBoards(userID, userRole)
      alert('نام بورد با موفقیت ویرایش شد')
    } else {
      alert('ویرایش بورد با خطا مواجه شد')
    }
  })
}

export function updateBoardDesc(name: string, newDesc: string, id: string, userID: string, userRole: string) {
  post('/updateBoard', {
    name: name,
    desc: newDesc,
    id: id,
  }).then(res => {
    if (res.success) {
      fetchBoards(userID, userRole)
      alert('توضیحات بورد با موفقیت ویرایش شد')
    } else {
      alert('ویرایش بورد با خطا مواجه شد')
    }
  })
}

export function removeBoard(id: string, userID: string, userRole: string) {
  post('/removeBoard', {
    _id: id
  }).then(res => {
    if (res.success) {
      fetchBoards(userID, userRole)
      setTimeout(() => alert('حذف بورد با موفقیت انجام شد'), 100)
    } else {
      alert('حذف بورد با خطا مواجه شد')
    }
  })
}

export function assignBoard(userID: string, boardID: string, userRole: string, name: string, desc: string) {
  post('/assignBoard', {
    userID: userID,
    boardID: boardID,
    name: name,
    desc: desc
  }).then(res => {
    if (res.success) {
      fetchBoards(userID, userRole)
      setTimeout(() => alert('کاربر با موفقیت به بورد اضافه شد'), 100)
    } else {
      alert('اضافه کردن کاربر با خطا مواجه شد')
    }
  })
}

export const boards = readonly(_boards)
