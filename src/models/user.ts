import { ref, readonly, watch } from 'vue'
import { get, post } from '@/utils/http'

const _users = ref([])
const _userID = ref(localStorage.getItem('userID') || '')
const _userName = ref(localStorage.getItem('userName') || '')
const _userRole = ref(localStorage.getItem('userRole') || '')

watch(_userID, (userID) => {
  localStorage.setItem('userID', userID)
})

watch(_userName, (userName) => {
  localStorage.setItem('userName', userName)
})

watch(_userRole, (userRole) => {
  localStorage.setItem('userRole', userRole)
})

export function setSpecify(id: string, name: string, role: string) {
  _userID.value = id
  _userName.value = name
  _userRole.value = role
}

export function fetchUsers() {
  get('/getUsers').then(res => {
    if (res.success) {
      _users.value = res.users
    } else {
      _users.value = []
    }
  })
}

export function addUser(username: string, password: string, role: string) {
  post('/createUser', {
    username: username,
    password: password,
    role: role
  }).then(res => {
    if (res.success) {
      fetchUsers()
      setTimeout(() => alert('کاربر با موفقیت ایجاد شد'), 100)
    } else {
      setTimeout(() => alert('افزودن کاربر با خطا مواجه شد'), 50)
    }
  })
}

export function editRole(id: string, role: string) {
  post('/editUser', {
    userID: id,
    role: role
  }).then(res => {
    if (res.success) {
      fetchUsers()
      alert('نقش کاربر با موفقیت ویرایش شد')
    } else {
      alert('ویرایش نقش با خطا مواجه شد')
    }
  })
}

export function removeUser(id: string) {
  post('/removeUser', {
    userID: id
  }).then(res => {
    if (res.success) {
      fetchUsers()
      setTimeout(() => alert('حذف کاربر با موفقیت انجام شد'), 100)
    } else {
      alert('حذف کاربر با خطا مواجه شد')
    }
  })
}

export const userID = readonly(_userID)
export const userName = readonly(_userName)
export const userRole = readonly(_userRole)
export const users = readonly(_users)
