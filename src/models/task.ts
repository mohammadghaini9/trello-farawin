import { get, post } from '@/utils/http'
import { ref, readonly } from 'vue'

const _tasks = ref([])
const _myTasks = ref([])
let temp
// get Tasks of server
export function fetchTasks() {
  get('/getTasks').then(res => {
    if (res.success) {
      _tasks.value = res.tasks
    } else {
      _tasks.value = []
    }
  })
}

export function fetchMyTasks(userID: string) {
  post('/getMyTasks', {
    userID: userID
  }).then(res => {
    temp = JSON.stringify(Object.assign({}, res))
    _myTasks.value = JSON.parse(temp)
  })
}

export function addTask(taskName: string, deadline: string, listFK: string) {
  post('/createTask', {
    name: taskName,
    deadline: deadline,
    listFK: listFK
  }).then(res => {
    if (res.success) {
      fetchTasks()
      setTimeout(() => alert('تسک با موفقیت ایجاد شد'), 100)
    } else {
      setTimeout(() => alert('ایجاد تسک با خطا مواجه شد'), 50)
    }
  })
}

export function updateTaskName(newName: string, deadline: string, id: string) {
  post('/updateTask', {
    name: newName,
    deadline: deadline,
    id: id,
  }).then(res => {
    if (res.success) {
      fetchTasks()
      alert('تسک با موفقیت ویرایش شد')
    } else {
      alert('ویرایش تسک با خطا مواجه شد')
    }
  })
}

export function updateTaskDeadline(name: string, newDeadline: string, id: string) {
  post('/updateTask', {
    name: name,
    deadline: newDeadline,
    id: id,
  }).then(res => {
    if (res.success) {
      fetchTasks()
      alert('تسک با موفقیت ویرایش شد')
    } else {
      alert('ویرایش تسک با خطا مواجه شد')
    }
  })
}

export function removeTask(id: string) {
  post('/removeTask', {
    _id: id
  }).then(res => {
    if (res.success) {
      fetchTasks()
      setTimeout(() => alert('حذف تسک با موفقیت انجام شد'), 100)
    } else {
      setTimeout(() => {
        alert('حذف تسک با خطا مواجه شد')
      }, 150)
    }
  })
}

export function assignTask(userID: string, taskID: string) {
  post('/assignTask', {
    userID: userID,
    taskID: taskID,
  }).then(res => {
    if (res.success) {
      setTimeout(() => alert('کاربر با موفقیت به تسک اضافه شد'), 100)
    } else {
      alert('اضافه کردن کاربر با خطا مواجه شد')
    }
  })
}

export const tasks = readonly(_tasks)
export const myTasks = readonly(_myTasks)
