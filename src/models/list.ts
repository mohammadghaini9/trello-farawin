import { post } from '@/utils/http'
import { ref, readonly } from 'vue'

const _lists = ref([])

// get lists of board with boardFK of server
export function fetchLists(boardId: string) {
  post('/getLists', { _id: boardId }).then(res => {
    if (res.success) {
      _lists.value = res.lists
    } else {
      _lists.value = []
    }
  })
}

export function addList(listTitle: string, boardFK: string) {
  post('/createList', {
    title: listTitle,
    boardFK: boardFK
  }).then(res => {
    if (res.success) {
      fetchLists(boardFK)
      // setTimeout(() => alert('لیست با موفقیت ایجاد شد'), 100)
    } else {
      setTimeout(() => alert('ایجاد لیست با خطا مواجه شد'), 50)
    }
  })
}

export function updateList(newTitle: string, id: string, boardFK: string) {
  post('/updateList', {
    title: newTitle,
    id: id,
  }).then(res => {
    if (res.success) {
      fetchLists(boardFK)
      alert('عنوان لیست با موفقیت ویرایش شد')
    } else {
      alert('ویرایش لیست با خطا مواجه شد')
    }
  })
}

export function removeList(id: string, boardFK: string) {
  post('/removeList', {
    _id: id
  }).then(res => {
    if (res.success) {
      fetchLists(boardFK)
      setTimeout(() => alert('حذف لیست با موفقیت انجام شد'), 100)
    } else {
      setTimeout(() => {
        alert('حذف لیست با خطا مواجه شد')
      }, 150)
    }
  })
}

export const lists = readonly(_lists)
