const router = require("express").Router();
const user = require("./handler/user");
const board = require("./handler/board");
const list = require("./handler/list");
const task = require("./handler/task");
const taskMember = require("./handler/taskMember");

const { isAuth } = require("./authMiddleware");
//user
router.post("/login", user.login);
router.post("/register", user.register);
router.post("/refresh-token", user.refreshToken);
router.post("/check", isAuth, user.check);
router.get("/getUsers", isAuth, user.getUsers);

//board
router.post("/createBoard", isAuth, board.create);
router.post("/updateBoard", isAuth, board.update);
// router.get("/getBoards", isAuth, board.listOfBoards);
router.post("/getUserBoards", isAuth, board.getUserBoards);
router.post("/removeBoard", isAuth, board.remove);
router.post("/assignBoard", isAuth, board.assignBoard);

//list
router.post("/createList", isAuth, list.create);
router.post("/updateList", isAuth, list.update);
router.post("/getLists", isAuth, list.lists);
router.post("/removeList", isAuth, list.remove);

//task
router.post("/createTask", isAuth, task.create);
router.post("/updateTask", isAuth, task.update);
router.get("/getTasks", isAuth, task.tasks);
router.post("/removetask", isAuth, task.remove);
router.post("/assignTask", isAuth, task.assignTask);

//taskMember
router.post("/getMyTasks", isAuth, taskMember.getMyTasks);

module.exports = router;
