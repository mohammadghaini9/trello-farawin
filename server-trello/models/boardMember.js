"use strict";
const { ObjectID } = require("mongodb");
const { insertOne, updateOne, findOne, deleteOne, findAll } = require("../db");

const remove = async (body) => {
  const res = await deleteOne("boardMember", { boardID: body._id });
  return { success: true, boardMember: res };
};

module.exports = { remove };