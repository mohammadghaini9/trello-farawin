"use strict"
const { ObjectID } = require("mongodb");
const { insertOne, updateOne, findOne, deleteOne, findAll } = require("../db");

// create one list in the board with boardFK ID 
const insert = async (body) => {
  if (!body.title) {
    return { success: false, error: "missing title" };
  }
  if (body.title.length < 3) {
    return { success: false, error: "invalid title" };
  }
  const res = await insertOne("list", { title: body.title, boardFK: body.boardFK});
  return { success: true, listID: res.insertedId };
};

// update one list of board
const update = async (body) => {
  if (!body.id || !isNaN(body.id)) {
    return { success: false, error: "invalid list" };
  }
  if (!body.title) {
    return { success: false, error: "missing title" };
  }
  if (body.title.length < 3) {
    return { success: false, error: "invalid title" };
  }
  const res = await updateOne(
    "list",
    { title: body.title },
    { _id: ObjectID(body.id) }
  );
  return { success: true, nModified: res.nModified };
};

// get All lists of board
const getAll = async (body) => {
  const res = await findAll("list", { boardFK: body._id });
  if (!res) {
    return { success: false, error: "lists not found" };
  }
  return { success: true, lists: res };
};

// remove one list with id
const remove = async (body) => {
  const res = await deleteOne("list", { _id: ObjectID(body._id) });
  return { success: true, list: res };
};

module.exports = { insert, update, remove, getAll };
