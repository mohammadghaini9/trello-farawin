"use strict"
const { ObjectID } = require("mongodb");
const { insertOne, updateOne, findOne, deleteOne, findAll } = require("../db");
const taskMember = require("./taskMember.js");

// create one task in the list with listFK ID 
const insert = async (body) => {
  if (!body.name) {
    return { success: false, error: "missing name" };
  }
  if (body.name.length < 3) {
    return { success: false, error: "invalid name" };
  }
  const res = await insertOne("task", { name: body.name, deadline: body.deadline, listFK: body.listFK});
  return { success: true, taskID: res.insertedId };
};

// update one task of list
const update = async (body) => {
  if (!body.id || !isNaN(body.id)) {
    return { success: false, error: "invalid task" };
  }
  if (!body.name) {
    return { success: false, error: "missing name" };
  }
  if (body.name.length < 3) {
    return { success: false, error: "invalid name" };
  }
  const res = await updateOne(
    "task",
    { name: body.name, deadline: body.deadline },
    { _id: ObjectID(body.id) }
  );
  return { success: true, nModified: res.nModified };
};

// get All tasks of list
const getAll = async () => {
  const res = await findAll("task");
  if (!res) {
    return { success: false, error: "tasks not found" };
  }
  return { success: true, tasks: res };
};

const get = async (body) => {
  const res = await findAll("task", { _id: ObjectID(body.taskID) });
  if (!res) {
    return { success: false, error: "tasks not found" };
  }
  return { success: true, tasks: res };
};

// remove one task with id
const remove = async (body) => {
  const res = await deleteOne("task", { _id: ObjectID(body._id) });
  if (res) {
    taskMember.remove(body);
  }
  return { success: true, task: res };
};

const assign = async (body) => {
  const res = await insertOne("taskMember", { userID: body.userID, taskID: body.taskID });
  return { success: true, taskMemberID: res.insertedId };
};

module.exports = { insert, update, remove, getAll, assign, get };