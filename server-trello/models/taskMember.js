"use strict";
const { ObjectID } = require("mongodb");
const { insertOne, updateOne, findOne, deleteOne, findAll, deleteMany } = require("../db");

const getAll = async (body) => {
  const res = await findAll("taskMember", { userID: body.userID });
  if (!res) {
    return { success: false, error: "tasks not found" };
  }
  return { success: true, myTasks: res };
}

const remove = async (body) => {
  const res = await deleteMany("taskMember", { taskID: body._id });
  return { success: true, taskMember: res };
};

module.exports = { remove, getAll };
