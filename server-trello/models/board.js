"use strict";
const { ObjectID } = require("mongodb");
const { insertOne, updateOne, findOne, deleteOne, findAll } = require("../db");

// create new board with name and desc
const insert = async (body) => {
  if (!body.name) {
    return { success: false, error: "missing name" };
  }
  if (body.name.length < 3) {
    return { success: false, error: "invalid name" };
  }
  if (!body.desc || body.desc.length < 8) {
    return { success: false, error: "invalid desc" };
  }
  const res = await insertOne("board", { name: body.name, desc: body.desc });
  return { success: true, name: body.name, boardID: res.insertedId };
};

// update one board
const update = async (body) => {
  if (!body.id || !isNaN(body.id)) {
    return { success: false, error: "invalid board" };
  }
  if (!body.name) {
    return { success: false, error: "missing name" };
  }
  if (body.name.length < 3) {
    return { success: false, error: "invalid name" };
  }
  if (!body.desc || body.desc.length < 8) {
    return { success: false, error: "invalid desc" };
  }
  const res = await updateOne("board", { 
    name: body.name, desc: body.desc },
    { _id: ObjectID(body.id) }
  );
  return { success: true, nModified: res.nModified };
};

// const get = async (body) => {
//   if (!body.id || body.id.length != 24) {
//     return { success: false, error: "invalid board" };
//   }
//   const res = await findOne("board", { _id: ObjectID(body.id) });
//   if (!res) {
//     return { success: false, error: "board not found" };
//   }
//   return { success: true, board: res };
// };

// get All boards array form
const getAll = async (body) => {
  const res = await findAll("board");
  if (!res) {
    return { success: false, error: "boards not found" };
  }
  return { success: true, boards: res };
};

const getBoardMembers = async (body) => {
  const res = await findAll("boardMember", { userID: body.userID });
  if (!res) {
    return { success: false, error: "boards not found" };
  }
  return { success: true, boards: res };
};

// delete one board with name and id 
const remove = async (body) => {
  if (!body._id || body._id.length != 24) {
    return { success: false };
  }
  const res = await deleteOne("board", { _id: ObjectID(body._id) });
  return { success: true, boards: res };
};

const assign = async (body) => {
  const res = await insertOne("boardMember", { userID: body.userID, boardID: body.boardID, name: body.name, desc: body.desc });
  return { success: true, boardMemberID: res.insertedId };
}

module.exports = { insert, update, getAll, remove, assign, getBoardMembers };
