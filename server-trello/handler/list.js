"use strict";
const list = require("../models/list");
const task = require("../models/task")

// create one list in the board with boardFK ID 
const create = async (req, res) => {
  try {
    const response = await list.insert(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

// update one list of board
const update = async (req, res) => {
  try {
    const response = await list.update(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

// get All list of board
const lists = async (req, res) => {
  try {
    const response = await list.getAll(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

// remove one list with id
const remove = async (req, res) => {
  try {
    const response = await list.remove(req.body);
    if (response) {
      const responseOfFindTasks  = await task.getAll();
      for (const tempTask of responseOfFindTasks.tasks) {
        if (req.body._id == tempTask.listFK) {
          await task.remove(tempTask);
        }
      }
    }
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};
module.exports = { create, update, lists, remove };
