"use strict";
const taskMember = require("../models/taskMember.js");
const task = require("./task.js");

const getMyTasks = async (req, res) => {
  try {
    const tempRes = [];
    const response = await taskMember.getAll(req.body);
    for (const tempTask of response.myTasks) {
      const tasks = await task.myTasks(tempTask)
      if(tasks.success) {
        tempRes.push(tasks.tasks);
      }
    }
    res.json(tempRes);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false });
  }
};

module.exports = { getMyTasks }