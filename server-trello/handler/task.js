"use strict";
const task = require("../models/task");

// create one task in the list with listFK ID 
const create = async (req, res) => {
  try {
    const response = await task.insert(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

// update one task of list
const update = async (req, res) => {
  try {
    const response = await task.update(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

// get All tasks of list
const tasks = async (req, res) => {
  try {
    const response = await task.getAll(req);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

const myTasks = async (req, res) => {
  try {
    const response = await task.get(req);
    return response;
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

// remove one task with id
const remove = async (req, res) => {
  try {
    const response = await task.remove(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

const assignTask = async(req, res) => {
  try {
    const response = await task.assign(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

module.exports = { create, update, remove, tasks, assignTask, myTasks };
