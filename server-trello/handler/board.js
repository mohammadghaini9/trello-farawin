"use strict";
const board = require("../models/board");
const listsFK = require("../models/list");
const tasksFK = require("../models/task");
const boardMember = require("../models/boardMember");
let boards = [];

// create new board with name and desc
const create = async (req, res) => {
  try {
    const response = await board.insert(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

// update one board
const update = async (req, res) => {
  try {
    const response = await board.update(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

// get All boards array form
const listOfBoards = async (req, res) => {
  try {
    const response = await board.getAll(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

const getUserBoards = async (req, res) => {
  if (req.body.userRole == 1) {
    try {
      const response = await board.getAll();
      res.json(response);
    } catch (err) {
      console.log(err);
      res.status(500).json({ success: false });
    }  
  } else {
    try {
      const response = await board.getBoardMembers(req.body);
      res.json(response);
    } catch (err) {
      console.log(err);
      res.status(500).json({ success: false });
    }
  }
}

//   boards = [];
//   try {
//     const response = await board.getUserBoards(req.body);
//     for (const tempBoard of response.boards) {
//       const temp = await board.getAll(tempBoard)
//       if (temp.success) {
//         boards.push(temp.boards)
//       }
//     }
//     res.json({ success: true, boards: boards });
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ success: false });
//   }

// delete one board with name and id
const remove = async (req, res) => {
  try {
    const response = await board.remove(req.body);
    boardMember.remove(req.body);
    if (response.success) {
      const responseOfFindLists = await listsFK.getAll(req.body);
      const responseOfFindTasks  = await tasksFK.getAll();
      for (const list of responseOfFindLists.lists) {
        const responseOfDeleteList = await listsFK.remove(list);
        if (responseOfDeleteList) {
          for (const task of responseOfFindTasks.tasks) {
            if (list._id == task.listFK) {
              await tasksFK.remove(task);
            }
          }
        }
      }
    }
    res.json(response);
  } catch (err) {
    console.log('remove board', err);
    res.status(500).json({ success: false })
  }
}

const assignBoard = async(req, res) => {
  try {
    const response = await board.assign(req.body);
    res.json(response);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
}

module.exports = { create, update, listOfBoards, remove, assignBoard, getUserBoards };
