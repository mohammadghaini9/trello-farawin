"use strict";
const jwt = require("jsonwebtoken");
const { getInstance, findAll } = require("../db");

const login = async (req, res) => {
  try {
    if (!req.body.username) {
      res.status(400).json({ success: false, error: "missing username" });
      return;
    }
    if (req.body.username.length < 3) {
      res.status(400).json({ success: false, error: "invalid username" });
      return;
    }
    if (!req.body.pass || req.body.pass.length < 8) {
      res.status(400).json({ success: false, error: "invalid pass" });
      return;
    }
    const db = await getInstance();
    const users = db.collection("users");
    const user = await users.findOne({
      username: req.body.username,
      pass: req.body.pass,
    });
    if (!user) {
      res.status(403).json({ success: false });
      return;
    }
    const resp = await generateToken(user._id, user.role);
    res.json(resp);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};
const register = async (req, res) => {
  try {
    if (!req.body.username) {
      res.status(400).json({ success: false, error: "missing username" });
      return;
    }
    if (req.body.username.length < 3) {
      res.status(400).json({ success: false, error: "invalid username" });
    }
    if (
      !req.body.pass ||
      req.body.pass.length < 8) {
      res.status(400).json({ success: false, error: "invalid pass" });
      return;
    }
    const db = await getInstance();
    const users = db.collection("users");
    const ok = await users.insertOne({
      username: req.body.username,
      pass: req.body.pass,
      email: req.body.email || "",
      role: 3,   // role = 3 => user is Viewer  
    });            // role = 2 => user is Creator
    if (!ok) {     // role = 1 => user is Admin
      res.status(500).json({ success: false });
      return;
    }
    res.json({ success: true });
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false });
  }
};

const getUsers = async (req, res) => {
  try {
    const response = await findAll("users");
    if (!res) {
      return { success: false, error: "users not found" };
    }
    res.json({ success: true, users: response });
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false, code: 1, error: "users not found" });
  }
}

const refreshToken = async (req, res) => {
  try {
    const refresh_token = req.body.refreshToken;
    if (!refresh_token) {
      res.status(401).json({ success: false, code: 1, error: "token not found" });
      return;
    }
    const decoded = jwt.verify(refresh_token, "40bil");
    const ok = await generateToken(decoded.id);
    res.json(ok);
  } catch (err) {
    console.log(err);
    res.status(500).json({ success: false, code: 2, error: "refresh token is invalid" });
  }
};

const check = (req, res) => {
  res.json({ success: true });
};

const generateToken = async (userID, userRole) => {
  const accessToken = jwt.sign({ id: userID }, "30bil", { expiresIn: "120min" });
  const rToken = jwt.sign({ id: userID }, "40bil", { expiresIn: "1d" });

  const db = await getInstance();
  const users = db.collection("users");
  const ok = await users.updateOne(
    { _id: userID },
    { $set: { refreshToken: rToken } }
  );
  if (!ok) {
    return { success: false, code: 1, error: "user not found with refresh token" };
  }
  return { success: true, _id: userID, role: userRole, access_token: accessToken, refresh_token: rToken };
};

module.exports = { login, register, refreshToken, check, getUsers };
