import { token, refreshToken, setToken, setRefreshToken } from '@/models/auth'
import { defineComponent } from 'vue'
import router from '@/router'

export function request(method: 'GET' | 'POST', url: string, data?: any) {
  return new Promise<any>((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.open(method, `/api${url}`)
    xhr.setRequestHeader('Content-Type', 'application/json')
    if (token.value) {
      xhr.setRequestHeader('access_token', token.value)
    }
    xhr.onload = async () => {
      try {
        const result = JSON.parse(xhr.responseText)
        console.log(result)
        if (result.code === 0 /* code = 0 => token is invalid or token is expired */) {
          try {
            const result = await request('POST', '/refresh-token', { refreshToken: refreshToken.value })
            setToken(result.access_token)
            setRefreshToken(result.refresh_token)
            request(method, url, data)
          } catch (err) {
            console.log('refresh is expired')
            router.push('/login')
          }
        }
        if (result.code === 2 /* code = 2 => refresh token is expired */) {
          router.push('/login')
        }
        resolve(result)
      } catch (err) {
        reject(err)
      }
    }
    xhr.onerror = reject
    xhr.send(JSON.stringify(data))
  })
}

export function post(url: string, data: any) {
  return request('POST', url, data)
}

export function get(url: string) {
  return request('GET', url)
}
