import { computed, readonly, ref, watch } from 'vue'

const _token = ref(localStorage.getItem('token') || '')
const _refreshToken = ref(localStorage.getItem('refreshToken') || '')

watch(_token, (token) => {
  localStorage.setItem('token', token)
})

export function setToken(value: string) {
  _token.value = value
}

watch(_refreshToken, (refreshToken) => {
  localStorage.setItem('refreshToken', refreshToken)
})

export function setRefreshToken(value: string) {
  _refreshToken.value = value
}

export const isLogin = computed(() => !!_token.value)
export const token = readonly(_token)
export const refreshToken = readonly(_refreshToken)
